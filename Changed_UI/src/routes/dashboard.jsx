import DashboardPage from "views/Dashboard/Dashboard.jsx";
import UserProfile from "views/UserProfile/UserProfile.jsx";
import TableList from "views/TableList/TableList.jsx";
import Typography from "views/Typography/Typography.jsx";
import Icons from "views/Icons/Icons.jsx";
import Maps from "views/Maps/Maps.jsx";
import NotificationsPage from "views/Notifications/Notifications.jsx";
import TravelRequest from "views/TravelRequest/TravelRequest.jsx";
import VendorUpdate from "views/VendorUpdate/VendorUpdate.jsx";
import ClaimSheet from "views/ClaimSheet/ClaimSheet.jsx";
import Wallet from "views/Wallet/Wallet.jsx";
import {
  Dashboard,
  Person,
  ContentPaste,
  LibraryBooks,
  BubbleChart,
  LocationOn,
  Notifications
} from "@material-ui/icons";

const dashboardRoutes = [

    {
      path: "/travelrequest",
      sidebarName: "Travel Request",
     // navbarName: "Travel Request",
      icon: Dashboard,
      component: TravelRequest
    },  
    {
      path: "/vendorupdate",
      sidebarName: "Vendor Update",
     // navbarName: "Vendor Update",
      icon: ContentPaste,
      component: VendorUpdate
    }, 

    {
      path: "/Claimsheet",
      sidebarName: "Final Settlement",
      // navbarName: "Claim Sheet",
      icon: ContentPaste,
      component: ClaimSheet
    }, 

    {
      path: "/Wallet",
      sidebarName: "Wallet",
      // navbarName: "Claim Sheet",
      icon: ContentPaste,
      component: Wallet
    }, 
  // {
  //   path: "/dashboard",
  //   sidebarName: "Dashboard",
  //   navbarName: "Material Dashboard",
  //   icon: Dashboard,
  //   component: DashboardPage
  // },
  // {
  //   path: "/user",
  //   sidebarName: "User Profile",
  //   navbarName: "Profile",
  //   icon: Person,
  //   component: UserProfile
  // },
  // {
  //   path: "/table",
  //   sidebarName: "Table List",
  //   navbarName: "Table List",
  //   icon: ContentPaste,
  //   component: TableList
  // },
  // {
  //   path: "/typography",
  //   sidebarName: "Typography",
  //   navbarName: "Typography",
  //   icon: LibraryBooks,
  //   component: Typography
  // },
  // {
  //   path: "/icons",
  //   sidebarName: "Icons",
  //   navbarName: "Icons",
  //   icon: BubbleChart,
  //   component: Icons
  // },
  // {
  //   path: "/maps",
  //   sidebarName: "Maps",
  //   navbarName: "Map",
  //   icon: LocationOn,
  //   component: Maps
  // },
  // {
  //   path: "/notifications",
  //   sidebarName: "Notifications",
  //   navbarName: "Notifications",
  //   icon: Notifications,
  //   component: NotificationsPage
  // },
  { redirect: true, path: "/", to: "/travelrequest", navbarName: "Redirect" }
];

export default dashboardRoutes;
