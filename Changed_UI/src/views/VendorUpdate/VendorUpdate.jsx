import React from "react";
import { Grid,Card,CardContent,Tooltip,IconButton, InputLabel,SnackbarContent,TextField,FormControlLabel,Checkbox,Dialog,DialogTitle,DialogContent,DialogActions,Slide} from "material-ui";
import classNames from "classnames";
import {
  ProfileCard,
  RegularCard,
  Button,
  CustomInput,
  ItemGrid
} from "components";
import AddCircle from '@material-ui/icons/AddCircle';
function Transition(props) {
    return <Slide direction="down" {...props} />;
  }
var arrayofrows_inovoice=[]   
var arrayofrows_paid=[]

class VendorUpdate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          
          form:{
          
          
          },
          addarray_invoice:[1],
          addarray_paid:[1],
          image_file:"",
          selected_Travel_id:"",
          alltravelrequests:[{emp_name:"Select",
          travel_request_id:""}],
          balanc_amount:0
      };
      

      this.onsubmitclaim=this.onsubmitclaim.bind(this);  
      this.alldatasneeded=this.alldatasneeded.bind(this);  
      this.cash_Update_wallet=this.cash_Update_wallet.bind(this);  
      }
    
  componentDidMount(){
    
    this.alldatasneeded()
    
  }  

  alldatasneeded(){


    fetch('http://localhost:3022/select/vendors/travelreq',
    {
      method:'GET',
      credentials : 'omit',
      headers: {
      
       'Accept': 'application/json',
       'Content-Type': 'application/json',
  
   },
    
    })
    .then(result=>result.json())
    .then((findres)=>{
      console.log(findres)
  
      this.setState({
 
        alltravelrequests:findres.vendors,
      
     })

      


     for(var i=0;i<this.state.alltravelrequests.length;i++){


      var stringofrecevied=this.state.alltravelrequests[i].emp_name  +" id:"+this.state.alltravelrequests[i].travel_request_id
      if(document.getElementById("travelrquest").value == stringofrecevied){

        this.setState({
          
          selected_Travel_id:this.state.alltravelrequests[i]
        
         })
          //  console.log("Super"+JSON.stringify(this.state.alltravelrequests[i]))
          // alert(JSON.stringify(this.state.alltravelrequests[i]))

      }
     } 
     
      //  this.changeHandler.bind(this,"travelrquest")

      //   var buffer = new ArrayBuffer(this.state.alltravelrequests[0].invoice.data);
      //   console.log( this.state.alltravelrequests[2].invoice.data);
        
      //   const buf = Buffer.from(this.state.alltravelrequests[1].invoice.data);
       
      //   let base64data = buf.toString('base64');

      //   let bufferOriginal = Buffer.from(this.state.alltravelrequests[1].invoice.data);
      //   let json = JSON.stringify(this.state.alltravelrequests[1].invoice.data);
      //   console.log(json);
      //   let bufferOriginal = Buffer.from(JSON.parse(json).data);


        var decoder = new TextDecoder('utf8');
        var b64encoded = btoa(decoder.decode(Buffer.from(this.state.alltravelrequests[0].invoice.data)));

        console.log("---------->>>>>>>>>"+b64encoded)
       // console.log(bufferOriginal);
       // console.log("------------>>>>>>>>>>",bufferOriginal)
        console.log(("++++++++++"))
       this.setState({
       image_file:b64encoded
      })
 
    })
    .catch((error) => {
      
      console.error(error);
 
   }); 
 
 
 


  }
  onsubmitclaim(ev){
    
    ev.preventDefault();

    var empid
    var travelrequestid
    for(var i=0;i<this.state.alltravelrequests.length;i++){


     var stringofrecevied=this.state.alltravelrequests[i].emp_name  +" id:"+this.state.alltravelrequests[i].travel_request_id
     if(document.getElementById("travelrquest").value == stringofrecevied){

         empid=this.state.alltravelrequests[i].emp_id
         travelrequestid=this.state.alltravelrequests[i].travel_request_id
         // alert(JSON.stringify(this.state.alltravelrequests[i]))

     }
    } 


    var USD_IV=0
    var MYR_IV=0
    var EUR_IV=0
    var AED_IV=0
    var SAR_IV=0
    var INR_IV=0
 
    var USD_PD=0
    var MYR_PD=0
    var EUR_PD=0
    var AED_PD=0
    var SAR_PD=0
    var INR_PD=0
 
    for(var index=0;index<this.state.addarray_invoice.length;index++){ 
   
   if(document.getElementById("currency_type"+index).value=="USD"){
 
     USD_IV= this.state.form["allocatedamount"+index]
 
   }
 
   if(document.getElementById("currency_type"+index).value=="MYR"){
 
     MYR_IV=this.state.form["allocatedamount"+index]
 
   }
   if(document.getElementById("currency_type"+index).value=="EUR"){
 
     EUR_IV=this.state.form["allocatedamount"+index]    
   }
   if(document.getElementById("currency_type"+index).value=="AED"){
 
     AED_IV= this.state.form["allocatedamount"+index]   
   }
   if(document.getElementById("currency_type"+index).value=="SAR"){
 
     SAR_IV= this.state.form["allocatedamount"+index]   
   }
   if(document.getElementById("currency_type"+index).value=="INR"){
 
     INR_IV=this.state.form["allocatedamount"+index]    
   }
 
  }
 
 

  for(var index=0;index<this.state.addarray_paid.length;index++){ 
   
    if(document.getElementById("currency_type_actual"+index).value=="USD"){
  
      USD_PD= this.state.form["actual_amount_paid"+index]
  
    }
  
    if(document.getElementById("currency_type_actual"+index).value=="MYR"){
  
      MYR_PD=this.state.form["actual_amount_paid"+index]
  
    }
    if(document.getElementById("currency_type_actual"+index).value=="EUR"){
  
      EUR_PD=this.state.form["actual_amount_paid"+index]    
    }
    if(document.getElementById("currency_type_actual"+index).value=="AED"){
  
      AED_PD= this.state.form["actual_amount_paid"+index]   
    }
    if(document.getElementById("currency_type_actual"+index).value=="SAR"){
  
      SAR_PD= this.state.form["actual_amount_paid"+index]   
    }
    if(document.getElementById("currency_type_actual"+index).value=="INR"){
  
      INR_PD=this.state.form["actual_amount_paid"+index]    
    }
  
   }
  
  

  console.log("USD_IV------->>>"+USD_IV)
  console.log("MYR_IV------->>>"+MYR_IV)
  console.log("EUR_IV------->>>"+EUR_IV)
  console.log("AED_IV------->>>"+AED_IV)
  console.log("SAR_IV------->>>"+SAR_IV)
  console.log("INR_IV------->>>"+INR_IV)   
    


  console.log("USD_PD------->>>"+USD_PD)
  console.log("MYR_PD------->>>"+MYR_PD)
  console.log("EUR_PD------->>>"+EUR_PD)
  console.log("AED_PD------->>>"+AED_PD)
  console.log("SAR_PD------->>>"+SAR_PD)
  console.log("INR_PD------->>>"+INR_PD)   

  
  var USD_BAL= USD_IV - USD_PD
  var MYR_BAL= MYR_IV - MYR_PD
  var EUR_BAL= EUR_IV - EUR_PD
  var AED_BAL= AED_IV - AED_PD
  var SAR_BAL= SAR_IV - SAR_PD
  var INR_BAL= INR_IV - INR_PD

 
  
  


    fetch('http://localhost:3022/update/vendor/travelrequest',
    {
      method:'POST',
      credentials : 'omit',
      headers: {
      
       'Accept': 'application/json',
       'Content-Type': 'application/json',
  
   },
    body:JSON.stringify({

       travelrequestid:travelrequestid,
       empid:empid,
       vendor:this.state.form.vendor,
       vendordate:this.state.form.transctiondate,
       allocatedamt:0,
       vendor_actual_amount_paid:0,
       currencytype:"none",
       currentrate:this.state.form.inr_amount,
       inramt:this.state.form.total_amount,
       payment_due_date:this.state.form.payment_due_date,
       invoice_number:this.state.form.invoice_number,
       invoice:"o",
       updatedby:this.props.username,
       updateddate:new Date().toISOString().slice(0, 10),
       flag:"final"
      


    })
    })
    .then(result=>result.json())
    .then((findres)=>{
      console.log(findres)
      console.log(("++++++++++"))
      if(findres.info=='update failure'){

        this.handleClickOpen("classicModal","Error Uploaded data")

      }else{

        this.handleClickOpen("classicModal","Successfully Saved")
        this.cash_Update_wallet(travelrequestid,"invoice_amount",USD_IV,MYR_IV,EUR_IV,AED_IV,SAR_IV,INR_IV)
        this.cash_Update_wallet(travelrequestid,"paid_amount",USD_PD,MYR_PD,EUR_PD,AED_PD,SAR_PD,INR_PD)
        this.cash_Update_wallet(travelrequestid,"balance",USD_BAL,MYR_BAL,EUR_BAL,AED_BAL,SAR_BAL,INR_BAL)
        
        this.alldatasneeded()
        let form=this.state.form
        this.state.form.vendor=""
        this.state.form.transctiondate=""
        this.state.form.allocatedamount=""
        this.state.form.inr_amount=""
        this.state.form.total_amount=""
        this.state.form.payment_due_date=""
        this.state.form.invoice_number="",
        this.state.form.actual_amount_paid=""
        this.state.form.currency_type=""

        
         for(var i=0;i<this.state.addarray_invoice.length;i++){
 
          this.state.form["allocatedamount"+i]=''   
          this.state.form["currency_type"+i]="USD"       
      
    
          }

          for(var i=0;i<this.state.addarray_paid.length;i++){
 
            this.state.form["actual_amount_paid"+i]=''   
            this.state.form["currency_type_actual"+i]="USD"       
          }
       
         this.setState({form}) 
         this.setState({addarray_invoice:[1],addarray_paid:[1]})


         this.setState({form})
        
      }
    })
    .catch((error) => {
      this.handleClickOpen("classicModal",error)
     console.error(error);
   });

  
  
  }

  
   cash_Update_wallet(travelrequestid,flag,USD,MYR,EUR,AED,SAR,INR){
 
    var Claim_sheet_Cash_Details=JSON.stringify({
     travelrequestid:travelrequestid,
     type_cash:flag,
     usd:USD,
     myr:MYR,
     eur:EUR,
     aed:AED,
     sar:SAR,
     inr:INR,
     flag:"final", 
     createdby:this.props.username,
     createddate:new Date().toISOString().slice(0, 10),
     updatedby:this.props.username,
     updateddate:new Date().toISOString().slice(0, 10),
 
    })
 
    fetch('http://localhost:3022/insert/cashupdates',
    {
      method:'POST',
      credentials : 'omit',
      headers: {
      
       'Accept': 'application/json',
       'Content-Type': 'application/json',
  
   },
    body:Claim_sheet_Cash_Details
    })
    .then(result=>result.json())
    .then((findres)=>{
      console.log(findres)
      console.log(("++++++++++"))
  
      if(findres.info=='insert failure'){
  
        //this.handleClickOpen("classicModal","Error Uploaded data")
  
      }else{
  
      //  this.handleClickOpen("classicModal","Successfully Saved")
        this.alldatasneeded()
  
  
        // let form=this.state.form
        // this.state.form.amount=""
        // this.state.form.currencytype=""
        
       let form=this.state.form 
       this.setState({form}) 
     
        
      }
  
    })
    .catch((error) => {
      this.handleClickOpen("classicModal",error)
     console.error(error);
   });
   }





   changeHandler (field, e) {
    
    let form = this.state.form;
    form[field] = e.target.value;
  
    if(field=="transctiondate"){

       
      var someDate = new Date(this.state.form.transctiondate.substr(0,10)); 
      console.log("Logs-->"+someDate)
      var numberOfDaysToAdd = 15;
      someDate.setDate(someDate.getDate() + numberOfDaysToAdd); 
      console.log("Logs-->"+someDate)


      this.state.form.payment_due_date=someDate.toISOString().slice(0, 10)
      // this.setState({
      //   payment_due_date

      // }) 
      this.setState({ form });


    }

    this.setState({balanc_amount:Number(+this.state.form.allocatedamount  -  +this.state.form.actual_amount_paid     ).toFixed(2)
    })
     //console.log("---------------->>>>>>>>>"+this.state.balanc_amount)


    
    for(var i=0;i<this.state.alltravelrequests.length;i++){


      var stringofrecevied=this.state.alltravelrequests[i].emp_name  +" id:"+this.state.alltravelrequests[i].travel_request_id
      if(document.getElementById("travelrquest").value == stringofrecevied){

        this.setState({
          
          selected_Travel_id:this.state.alltravelrequests[i]
        
         })
          //  console.log("Super"+JSON.stringify(this.state.alltravelrequests[i]))
          // alert(JSON.stringify(this.state.alltravelrequests[i]))

      }
     } 
    
   }
   handleClose(modal) {

    var x = [];
    x[modal] = false;
    this.setState(x);

  }

handleClickOpen(modal,message) {

    console.log("message"+message)
    var x = [];
    x[modal] = true;
    this.setState(x);
    this.setState({
      alertmessage:message
    });

  }
  
  addrow(action){


    if(action=="invoice"){
      arrayofrows_inovoice=this.state.addarray_invoice 
      arrayofrows_inovoice.push(1)
       this.setState({
  
        addarray_invoice:arrayofrows_inovoice
  
     })
    }else{

      arrayofrows_paid=this.state.addarray_paid 
      arrayofrows_paid.push(1)
       this.setState({
  
        addarray_paid:arrayofrows_paid
  
     })

    }
  }
  render() {
    const { classes } = this.props;
    return (
      <div >

            <Dialog
                
                fullWidth
              
                open={this.state.classicModal}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => this.handleClose("classicModal")}
                aria-labelledby="classic-modal-slide-title"
                aria-describedby="classic-modal-slide-description"

                >

                <DialogTitle
                id="classic-modal-slide-title"
                disableTypography
            
                >

                    
                <h4 >Info </h4>
                </DialogTitle>

                <DialogContent
                    
                id="classic-modal-slide-description"
             

                >
                <p> {this.state.alertmessage}</p>

 </DialogContent>
         <DialogActions >
           {/* <Button color="transparent" simple>
             Nice Button
           </Button> */}
           <Button
             onClick={() => this.handleClose("classicModal")}
             color="transparent"
             simple
           >
             Close
           </Button>
         </DialogActions>
       </Dialog>

  <Grid container>
        <ItemGrid xs={12} sm={12} md={8}>
          <RegularCard
            cardTitle="Vendor Update"
            cardSubtitle="Complete your profile"
            content={
<div>


  
<div>

{/* <img src={"data:image/jpeg;" + this.state.image_file} /> */}
 <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">All Travel Request</label>
    <select 
      value={this.state.form.travelrquest}
      onChange={this.changeHandler.bind(this,"travelrquest")}
      onClick={this.changeHandler.bind(this,"travelrquest")}
      id="travelrquest"
      class="form-control" id="travelrquest" aria-describedby="emailHelp"  >
   

      
      {this.state.alltravelrequests.map((row,index ) => {
                                
                                return(
                              
                                  <option>{this.state.alltravelrequests[index].emp_name + " id:"+this.state.alltravelrequests[index].travel_request_id}</option>
                                  
                              
                                ) })}
   
      </select>
      </div>
  
 
  
      <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Vendor</label>
    <input type="email"   value={this.state.form.vendor}
                  onChange={this.changeHandler.bind(this,"vendor")} id="vendor" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"  placeholder=""/>
      </div>
              
 

 
 <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Invoice Date</label>
    <input type="email"    value={this.state.form.transctiondate}
                 
                 onChange={this.changeHandler.bind(this,"transctiondate")}
                  id="transctiondate"
                  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" type="date" placeholder=""/>
      </div>


  <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Payment due date </label>
    <input type="email"      value={this.state.form.payment_due_date}
                  onChange={this.changeHandler.bind(this,"payment_due_date")}
                  id="payment_due_date"
                  class="form-control" id="payment_due_date" aria-describedby="emailHelp" type="date" placeholder=""/>
      </div>

     
 {/* <div class="form-row">
    <div class="form-group col-md-6">
      <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Amount to be paid (Invoice Amount)</label>
    <input type="email"      value={this.state.form.allocatedamount}
                  onChange={this.changeHandler.bind(this,"allocatedamount")}
                  id="allocatedamount"
                  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" type="number" placeholder=""/>

    </div>
    <div class="form-group col-md-3">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Currency </label>

    </div>

    <div STYLE="margin-top:24px" class="form-group col-md-3">


    </div>
  </div> */}
  
  <table class="table">
    <thead>
      <tr>
        <th>Amount to be paid (Invoice Amount)</th>
        <th>Currency</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

         {this.state.addarray_invoice.map((row,index ) => {
                                     
           
                                     console.log('row'+row)
           
                                     console.log('Index of the value'+index)
                                     return(
      <tr>
        <td>  <input type="email"      value={this.state.form["allocatedamount"+index]}
                  onChange={this.changeHandler.bind(this,"allocatedamount"+index)}
                  id={"allocatedamount"+index}
                  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" type="number" placeholder=""/></td>
        <td>
    <select 
      value={this.state.form["currency_type"+index]}
      onChange={this.changeHandler.bind(this,"currency_type"+index)}
      onClick={this.changeHandler.bind(this,"currency_type"+index)}
      class="form-control" id={"currency_type"+index} aria-describedby="emailHelp"  >
 
                             
               <option>USD</option>
               <option>MYR</option>
               <option>EUR</option>
               <option>AED</option>
               <option>SAR</option>
               <option>INR</option>                          
                              
      </select></td>
        <td>    <Tooltip id="tooltip-icon" title="Add">
                   <IconButton 
                   onClick={this.addrow.bind(this,"invoice")}
                    aria-label="Add">
                     <AddCircle />
                   </IconButton>
                 </Tooltip>
</td>
      </tr>
          ) })}
    </tbody>
  </table>


  <table class="table">
    <thead>
      <tr>
        <th>Actual Amount Paid</th>
        <th>Currency</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

    {this.state.addarray_paid.map((row,index ) => {

  return(
      <tr>
        <td>  <input type="email"      value={this.state.form["actual_amount_paid"+index]}
                  onChange={this.changeHandler.bind(this,"actual_amount_paid"+index)}
                  id={"actual_amount_paid"+index}
                  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" type="number" placeholder=""/></td>
        <td>
    <select 
      value={this.state.form.currency_type_actual}
      onChange={this.changeHandler.bind(this,"currency_type_actual"+index)}
      onClick={this.changeHandler.bind(this,"currency_type_actual"+index)}
      class="form-control" id={"currency_type_actual"+index} aria-describedby="emailHelp"  >
 
                             
               <option>USD</option>
               <option>MYR</option>
               <option>EUR</option>
               <option>AED</option>
               <option>SAR</option>
               <option>INR</option>                          
                              
      </select></td>
        <td>    <Tooltip id="tooltip-icon" title="Add">
                   <IconButton 
                    onClick={this.addrow.bind(this,"paid")}
                    aria-label="Add">
                     <AddCircle />
                   </IconButton>
                 </Tooltip>
</td>
      </tr>
    )})}
    </tbody>
  </table>


 {/* <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Actual Amount paid </label>
    <input type="email"      value={this.state.form.actual_amount_paid}
                  onChange={this.changeHandler.bind(this,"actual_amount_paid")}
                  id="actual_amount_paid"
                  class="form-control" id="actual_amount_paid" aria-describedby="emailHelp" type="number" placeholder=""/>
      </div> */}

 <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Invoice Number </label>
    <input type="email"      value={this.state.form.invoice_number}
                  onChange={this.changeHandler.bind(this,"invoice_number")}
                  id="allocatedamount"
                  class="form-control" id="invoice_number" aria-describedby="emailHelp" type="number" placeholder=""/>
      </div>
  
 
 <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Current Currency rate</label>
    <input type="email"          value={this.state.form.inr_amount}
                  onChange={this.changeHandler.bind(this,"inr_amount")}
                  id="inr_amount"
                  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" type="number" placeholder=""/>
      </div>
    
      <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">INR amount</label>
    <input type="email"           value={this.state.form.total_amount}
                onChange={this.changeHandler.bind(this,"total_amount")}
                  id="total_amount"
                  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" type="number" placeholder=""/>
      </div>
 

 

    <Button  style={{marginTop:50}} onClick={this.onsubmitclaim} color="success">Sumbit</Button>
            
        </div>


</div>

            }
            />
  </ItemGrid >

       <ItemGrid xs={12} sm={12} md={4}>


       

           <Card >

           <CardContent>

              <strong> Upload Invoice </strong>
              <br/>
           
              <div class="form-group">

    <input type="email"     
                  id="invoice_pdf"
                  ref={(ref) => { this.uploadInput = ref; }}
                  STYLE="margin-top:25px;"  type="file" placeholder=""/>
      </div>
           </CardContent>
          </Card> 
          <Card  STYLE="margin-top:50px;">

<CardContent>

   <strong> Requested Amount </strong>
   <br/>
   {this.state.selected_Travel_id.grand_total}
</CardContent>
</Card> 
          {/* <ProfileCard
            
          
            title="Requested Amount"
            description="Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is..."
           
          /> */}
      
        </ItemGrid>
        
  </Grid >

      </div>
    );
  }
}

export default VendorUpdate;
