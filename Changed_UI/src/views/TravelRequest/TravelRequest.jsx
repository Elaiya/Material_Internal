import React from "react";
import { Grid,Tooltip,IconButton, InputLabel,SnackbarContent,TextField,FormControlLabel,Checkbox,Dialog,DialogTitle,DialogContent,DialogActions,Slide} from "material-ui";
import classNames from "classnames";
import {
  ProfileCard,
  RegularCard,
  Button,
  CustomInput,
  ItemGrid
} from "components";

import AddCircle from '@material-ui/icons/AddCircle';
import { AddAlert } from "@material-ui/icons";
import avatar from "assets/img/faces/marc.jpg";
function Transition(props) {
    return <Slide direction="down" {...props} />;
  }
 var array_conv=[] 
 var array_allowance=[]
class TravelRequest extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            addarray:[1],
            addarray_conv:[1],
            addarray_allowance:[1], 
            tc: false,
            checked: [24, 22],
            selectedEnabled: "b",
            datevalidation:"",
            classicModal: false,
            checkedA: true,
            checkedB: true,
            alertmessage:"",
            allemployeeFR:[
              {emp_name:'Select'}
            ],
            allcountryFR:[],
            selectedemployee:"",
            alldistinctcountries:[{country:"Select"}],
            selecctedtype:"",
            form:{
              totaldiam:'',
            
            },
      
      
            alltravelrequests:[{emp_name:"Select",
            travel_request_id:""}],
      
            allclaiamedrequest:[],
            
      
      
            MYR:"",
            EUR:"",
            AED:"",
            SAR:"",
            INR:"",
            
      
            
          };   
        
          this.totalcostcalci= this.totalcostcalci.bind(this);
          this.alldatasneeded=this.alldatasneeded.bind(this)
          this.allconverstioncosts=this.allconverstioncosts.bind(this)
          this.onsubmittravel=this.onsubmittravel.bind(this);
        } 

 showNotification(place) {
            var x = [];
            x[place] = true;
            this.setState(x);
            setTimeout(
              function() {
                x[place] = false;
                this.setState(x);
              }.bind(this),
              6000
            );
          }
totalcostcalci(){

    let form = this.state.form;
    var oneDay = 24*60*60*1000;
    var data_start=new Date(this.state.form.startdate)
    var date_end=new Date(this.state.form.enddate)
    var diffDays = Math.round(Math.abs((data_start.getTime() - date_end.getTime())/(oneDay)));
    var alldays=+diffDays + 1
        
    this.state.form.totalcost=  +this.state.form.totaldiam * alldays
    this.setState({ form });

}  

handleClose(modal) {

    var x = [];
    x[modal] = false;
    this.setState(x);

  }

  addrow(action){

    if(action=="conv"){
      array_conv=this.state.addarray_conv 
      array_conv.push(1)
       this.setState({
  
        addarray_conv:array_conv
  
     })
    }else{

      array_allowance=this.state.addarray_allowance 
      array_allowance.push(1)
       this.setState({
  
        addarray_allowance:array_allowance
  
     })

    }
  }
handleClickOpen(modal,message) {

    console.log("message"+message)
    var x = [];
    x[modal] = true;
    this.setState(x);
    this.setState({
      alertmessage:message
    });

  }
onsubmittravel(){

    fetch('http://localhost:3022/insert/manager/travelrequest',
    {
      method:'POST',
      credentials : 'omit',
      headers: {
      
       'Accept': 'application/json',
       'Content-Type': 'application/json',
  
   },
    body:JSON.stringify({
    
     empid:this.state.selectedemployee.emp_id,
     consultanttypeid:this.state.selecctedtype.consultant_type_id,
     country:this.state.form.countryselected,
     eligiblediem:this.state.form.totaldiam,
     startdate:this.state.form.startdate,
     enddate:this.state.form.enddate,
     totaldiem:this.state.form.totalcost,
     claimenddate:this.state.form.startdate,
     balanceamt:0,
     special_allowance:this.state.form.specialallowance,
     convenience:this.state.form.Convenience,
     grandtotal:this.state.form.grandtotal,
     remarks:this.state.form.remarks,
   
     createdby:this.props.username,
     createddate:new Date().toISOString().slice(0, 10),
     updatedby:this.props.username,
     updateddate:new Date().toISOString().slice(0, 10),

    })
    })
    .then(result=>result.json())
    .then((findres)=>{
      console.log(findres)
      console.log(("++++++++++"))
      if(findres.info=='insert failure'||findres.info=='update failure'){

        this.handleClickOpen("classicModal","Error Uploaded data")

      }else{

        this.handleClickOpen("classicModal","Successfully Saved")
        this.alldatasneeded()
        let form=this.state.form
      
       this.state.form.startdate=""
       this.state.form.enddate=""
       this.state.form.totalcost=""
       this.state.form.startdate=""
       this.state.form.specialallowance=""
       this.state.form.Convenience=""
       this.state.form.grandtotal=""
       this.state.form.remarks=""
        this.setState({form})
        
      }
    })
    .catch((error) => {
      this.handleClickOpen("classicModal",error)
     console.error(error);
   });

  }

alldatasneeded(){


    //alert("mounted")
    fetch('http://localhost:3022/select/master/travelexpense',
    {
      method:'GET',
      credentials : 'omit',
      headers: {
      
       'Accept': 'application/json',
       'Content-Type': 'application/json',
  
   },
    
    })
    .then(result=>result.json())
    .then((findres)=>{
      console.log(findres)
  
      this.setState({

        allemployeeFR:findres.emp,
        allcountryFR:findres.country,
        alldistinctcountries:findres.distinctcountry

       })
     
       console.log(("++++++++++"))

    })
    .catch((error) => {
      
      console.error(error);

   });

   
   fetch('http://localhost:3022/select/vendors/travelreq',
   {
     method:'GET',
     credentials : 'omit',
     headers: {
     
      'Accept': 'application/json',
      'Content-Type': 'application/json',
 
  },
   
   })
   .then(result=>result.json())
   .then((findres)=>{
     console.log(findres)
 
     this.setState({

       alltravelrequests:findres.vendors,
     
    })
    
      console.log(("++++++++++"))

   })
   .catch((error) => {
     
     console.error(error);

  }); 




  fetch('http://localhost:3022/select/allclaimed',
   {
     method:'GET',
     credentials : 'omit',
     headers: {
     
      'Accept': 'application/json',
      'Content-Type': 'application/json',
 
  },
   
   })
   .then(result=>result.json())
   .then((findres)=>{
     console.log(findres)
 
     this.setState({

       allclaiamedrequest:findres.claimed,
     
    })
    
      console.log(("++++++++++"))

   })
   .catch((error) => {
     
     console.error(error);

  }); 


 }
 allconverstioncosts(){
    fetch('http://free.currencyconverterapi.com/api/v5/convert?q=USD_INR&compact=y',
    {
      method:'GET',
      credentials : 'omit',
      headers: {
      
       'Accept': 'application/json',
       'Content-Type': 'application/json',
  
   },
    
    })
    .then(result=>result.json())
    .then((findres)=>{
      console.log(findres)
  
      // alert(findres.USD_INR.val)
       this.setState({ form:{
         
         inr_amount:findres.USD_INR.val
 
       }});
       console.log(("++++++++++"))
 
    })
    .catch((error) => {
      
      console.error(error);
 
   });
   
   
 
 
 
 
   fetch('http://free.currencyconverterapi.com/api/v5/convert?q=MYR_USD&compact=y',
   {
     method:'GET',
     credentials : 'omit',
     headers: {
     
      'Accept': 'application/json',
      'Content-Type': 'application/json',
 
  },
   
   })
   .then(result=>result.json())
   .then((findres)=>{
     console.log(findres)
 
     // alert(findres.USD_INR.val)
      this.setState({ 
        
        MYR:findres.MYR_USD.val
 
     });
      console.log(("++++++++++"))
 
   })
   .catch((error) => {
     
     console.error(error);
 
  });
 
  fetch('http://free.currencyconverterapi.com/api/v5/convert?q=EUR_USD&compact=y',
  {
    method:'GET',
    credentials : 'omit',
    headers: {
    
     'Accept': 'application/json',
     'Content-Type': 'application/json',
 
 },
  
  })
  .then(result=>result.json())
  .then((findres)=>{
    console.log(findres)
 
    // alert(findres.USD_INR.val)
     this.setState({ 
       
       EUR:findres.EUR_USD.val
 
    });
     console.log(("++++++++++"))
 
  })
  .catch((error) => {
    
    console.error(error);
 
 });
 
 fetch('http://free.currencyconverterapi.com/api/v5/convert?q=SAR_USD&compact=y',
 {
   method:'GET',
   credentials : 'omit',
   headers: {
   
    'Accept': 'application/json',
    'Content-Type': 'application/json',
 
 },
 
 })
 .then(result=>result.json())
 .then((findres)=>{
   console.log(findres)
 
   // alert(findres.USD_INR.val)
    this.setState({ 
      
      SAR:findres.SAR_USD.val
 
   });
    console.log(("++++++++++"))
 
 })
 .catch((error) => {
   
   console.error(error);
 
 });
 
 
 fetch('http://free.currencyconverterapi.com/api/v5/convert?q=AED_USD&compact=y',
 {
  method:'GET',
  credentials : 'omit',
  headers: {
  
   'Accept': 'application/json',
   'Content-Type': 'application/json',
 
 },
 
 })
 .then(result=>result.json())
 .then((findres)=>{
  console.log(findres)
 
  // alert(findres.USD_INR.val)
   this.setState({ 
     
     AED:findres.AED_USD.val
 
  });
   console.log(("++++++++++"))
 
 })
 .catch((error) => {
  
  console.error(error);
 
 });
 
 
 
 fetch('http://free.currencyconverterapi.com/api/v5/convert?q=INR_USD&compact=y',
 {
  method:'GET',
  credentials : 'omit',
  headers: {
  
   'Accept': 'application/json',
   'Content-Type': 'application/json',
 
 },
 
 })
 .then(result=>result.json())
 .then((findres)=>{
  console.log(findres)
 
  // alert(findres.USD_INR.val)
   this.setState({ 
     
     INR:findres.INR_USD.val
 
  });
   console.log(("++++++++++"))
 
 })
 .catch((error) => {
  
  console.error(error);
 
 });

}
datechange(field,e){

    let form = this.state.form;
    form[field] = e.target.value;
    this.setState({ form });


    var data_start=new Date(this.state.form.startdate)
    var date_end=new Date(this.state.form.enddate)
    
    
    if(date_end.getTime() < data_start.getTime()){

        this.state.form.enddate=""
        this.setState({ form });
        this.setState({
            datevalidation:"Please enter a valid date"
        })

    }else{
        
        this.totalcostcalci()
        this.setState({
        datevalidation:""
        })
    }
        
}       

handleChange = name => event => {

    this.setState({ [name]: event.target.checked });
    
    let form = this.state.form;
    
    if(this.state.checkedA==true){
     
      this.state.form.totaldiam=this.state.selecctedtype.without_food
      this.setState({ form });
      this.totalcostcalci()
    }else{

     
      this.state.form.totaldiam=this.state.selecctedtype.with_food
      this.setState({ form });
      this.totalcostcalci()
    }
    //console.log("ncjdebbejhb due"+this.state.checkedA )


  };
changeHandler (field, e) {
   
    

           let form = this.state.form;
           form[field] = e.target.value;
           this.setState({ form });

          this.state.form.grandtotal=Number(+this.state.form.totalcost   +  +this.state.form.specialallowance  +  +this.state.form.Convenience).toFixed(2)

          this.setState({ form });
        // let form = this.state.form;
         if(field=="totalcost"||field=="inr_amount"||field=="total_amount"){
     
           let form = this.state.form;
           form[field] = e.target.value;
           this.setState({ form });
     
         }else if(field=="claimedrequest"){
     
     
     
     
         }
     
         // currentamount  currencytype  usdamount Malasiya
         // else if(field=="currentamount"||field=="currencytype"){
     
         //   let form = this.state.form;
         //   form[field] = e.target.value;
         //   this.setState({ form });
     
           
     
     
     
         // }
         
         
         else{
     
           let form = this.state.form;
           form[field] = e.target.value;
           this.setState({ form });
           
       
           

           this.setState({ form });  
           
           
            
           for(var i=0;i<this.state.allemployeeFR.length;i++){
     
              
             try{
             if(document.getElementById("selectedvalue").value==this.state.allemployeeFR[i].emp_name){
               
               for(var j=0;j<this.state.allcountryFR.length;j++){
       
                 if(this.state.allemployeeFR[i].consultant_type_id==this.state.allcountryFR[j].consultant_type_id && this.state.allcountryFR[j].country==document.getElementById("countryselected").value){
       
                         this.setState({
                           selecctedtype:this.state.allcountryFR[j],
                           selectedemployee:this.state.allemployeeFR[i]
                         })
                
                   
                         
                     if(this.state.checkedA==true){
       
                         let form = this.state.form;
                         this.state.form.totaldiam=this.state.allcountryFR[j].with_food
                         this.setState({ form });
                         this.totalcostcalci()
          
                     
                     }else{
       
                         let form = this.state.form;
                         this.state.form.totaldiam=this.state.allcountryFR[j].without_food
                         this.setState({ form });
                         this.totalcostcalci()
          
                   
                     }
                   }
                }
              }
             }catch(e){
                console.log("Runtime Error "+e)
     
             }
            }
     
          
            // this.state.form.total_amount=  +this.state.form.allocatedamount   *   +this.state.form.inr_amount
            // this.setState({ form });
             
     
     
     
     
         }
               }
       
  componentDidMount(){

    this.alldatasneeded()
   //this.allconverstioncosts()
  }             
  render(){  

    

  return (

    
    <div>
  {/* <SnackbarContent
                  message={
                    "This is a notification with close button and icon and have many lines. You can see that the icon and the close button are always vertically aligned. This is a beautiful notification. So you don't have to worry about the style."
                  }
                  close
                  icon={AddAlert}
                /> */}
                <Dialog
                
                fullWidth
              
                open={this.state.classicModal}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => this.handleClose("classicModal")}
                aria-labelledby="classic-modal-slide-title"
                aria-describedby="classic-modal-slide-description"

                >

                <DialogTitle
                id="classic-modal-slide-title"
                disableTypography
            
                >

                    
                <h4 >Info </h4>
                </DialogTitle>

                <DialogContent
                    
                id="classic-modal-slide-description"
             

                >
                <p> {this.state.alertmessage}</p>

 </DialogContent>
         <DialogActions >
           {/* <Button color="transparent" simple>
             Nice Button
           </Button> */}
           <Button
             onClick={() => this.handleClose("classicModal")}
             color="transparent"
             simple
           >
             Close
           </Button>
         </DialogActions>
       </Dialog>
  
      <Grid container>
        <ItemGrid xs={12} sm={12} md={8}>
          <RegularCard
          
            cardTitle="Travel Request"
            cardSubtitle="Complete your profile"
            content={

                
                <div id="inputs">
                


  <form>
  <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Employee</label>
    <select 
      value={this.state.form.selectedvalue}
      onChange={this.changeHandler.bind(this,"selectedvalue")} 
      class="form-control" id="selectedvalue" aria-describedby="emailHelp"  >
   

      
      {this.state.allemployeeFR.map((row,index ) => {
                             
                             return(
                           
                               <option>{this.state.allemployeeFR[index]['emp_name']}</option>
                               
                           
                             ) })}
   
      </select>
      </div>
   
    
      <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Country</label>
    <select 
      value={this.state.form.countryselected}
      onChange={this.changeHandler.bind(this,"countryselected")}
      class="form-control" id="countryselected" aria-describedby="emailHelp"  >
   

     {this.state.alldistinctcountries.map((row,index ) => {
                                   
                                   return(
                                 
                                     <option>{this.state.alldistinctcountries[index].country}</option>
                                     
                                 
                                   ) })}
                   
   
      </select>
      </div>



      <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Start date</label>
    <input type="email" value={this.state.form.startdate} 
                   onChange={this.datechange.bind(this,"startdate")} class="form-control" id="startdate" aria-describedby="emailHelp" type="date" placeholder=""/>
      </div>


      
      <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">End date</label>
    <input type="email"      value={this.state.form.enddate} 
                     onChange={this.datechange.bind(this,"enddate")} class="form-control" id="enddate" aria-describedby="emailHelp" type="date" placeholder=""/>
      </div>
      <strong STYLE="color:red;" > {this.state.datevalidation}  </strong>
   </form>

            
           
                                 
                 
                    <tr>
                    <FormControlLabel
                     control={
                       <Checkbox
                              
                       checked={this.state.checkedA}
                       onChange={this.handleChange('checkedA')}
                       value="checkedA"
                     />
                     }
                     label="Food"
                      />
                     {/* <FormControlLabel
                     control={
                       <Checkbox
                              
                       checked={this.state.checkedB}
                       onChange={this.handleChange('checkedB')}
                       value="checkedB"
                     />
                     }
                     label="Accommodation"
                      /> */}
                      </tr>
                  
                      <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Eligiblity per diem</label>
    <input type="email"   value={this.state.form.totaldiam}
 class="form-control" id="totaldiam" aria-describedby="emailHelp" placeholder=""/>
      </div>


      <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Total Cost</label>
    <input type="email"  
 class="form-control"  value={this.state.form.totalcost}
 onChange={this.changeHandler.bind(this,"totalcost")} id="totalcost" aria-describedby="emailHelp" placeholder=""/>
      </div>      
{/*     
      <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Special allowance</label>
    <input type="email"  
 class="form-control"   value={this.state.form.specialallowance}
 onChange={this.changeHandler.bind(this,"specialallowance")} id="specialallowance" aria-describedby="emailHelp" placeholder=""/>
      </div>  */}


      {/* <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Convenience</label>
    <input type="email"  
 class="form-control"  value={this.state.form.Convenience}
 onChange={this.changeHandler.bind(this,"Convenience")}   id="Convenience" aria-describedby="emailHelp" placeholder=""/>
      </div>           */}
     <table class="table">
    <thead>
      <tr>
        <th>Special Allowance</th>
        <th>Currency</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

         {this.state.addarray_allowance.map((row,index ) => {
                                     
           
                                     console.log('row'+row)
           
                                     console.log('Index of the value'+index)
                                     return(
      <tr>
        <td>  <input type="email"      value={this.state.form["special_allowance"+index]}
                  onChange={this.changeHandler.bind(this,"special_allowance"+index)}
                  id={"special_allowance"+index}
                  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" type="number" placeholder=""/></td>
        <td>
    <select 
      value={this.state.form["currency_type_spcl"+index]}
      onChange={this.changeHandler.bind(this,"currency_type_spcl"+index)}
      onClick={this.changeHandler.bind(this,"currency_type_spcl"+index)}
      class="form-control" id={"currency_type_spcl"+index} aria-describedby="emailHelp"  >
 
                             
               <option>USD</option>
               <option>MYR</option>
               <option>EUR</option>
               <option>AED</option>
               <option>SAR</option>
               <option>INR</option>                          
                              
      </select></td>
        <td>    <Tooltip id="tooltip-icon" title="Add">
                   <IconButton 
                   onClick={this.addrow.bind(this,"allowance")}
                    aria-label="Add">
                     <AddCircle />
                   </IconButton>
                 </Tooltip>
</td>
      </tr>
          ) })}
    </tbody>
  </table>

    <table class="table">
    <thead>
      <tr>
        <th>Convenience</th>
        <th>Currency</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>

         {this.state.addarray_conv.map((row,index ) => {
                                     
           
                                     console.log('row'+row)
           
                                     console.log('Index of the value'+index)
                                     return(
      <tr>
        <td>  <input type="email"      value={this.state.form["convience"+index]}
                  onChange={this.changeHandler.bind(this,"convience"+index)}
                  id={"convience"+index}
                  class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" type="number" placeholder=""/></td>
        <td>
    <select 
      value={this.state.form["currency_type_conv"+index]}
      onChange={this.changeHandler.bind(this,"currency_type_conv"+index)}
      onClick={this.changeHandler.bind(this,"currency_type_conv"+index)}
      class="form-control" id={"currency_type_conv"+index} aria-describedby="emailHelp"  >
 
                             
               <option>USD</option>
               <option>MYR</option>
               <option>EUR</option>
               <option>AED</option>
               <option>SAR</option>
               <option>INR</option>                          
                              
      </select></td>
        <td>    <Tooltip id="tooltip-icon" title="Add">
                   <IconButton 
                   onClick={this.addrow.bind(this,"conv")}
                    aria-label="Add">
                     <AddCircle />
                   </IconButton>
                 </Tooltip>
</td>
      </tr>
          ) })}
    </tbody>
  </table>
  <div class="form-group">
    <label for="exampleInputEmail1"   STYLE=" font-weight: bold;color:black;">Grand Total</label>
    <input type="email"  
 class="form-control" disabled  value={this.state.form.grandtotal}
 onChange={this.changeHandler.bind(this,"grandtotal")}
 margin="normal"   id="grandtotal" aria-describedby="emailHelp" placeholder=""/>
      </div> 

     <div class="form-group">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Remarks</label>
    <input type="email"  
 class="form-control"     value={this.state.form.remarks}
 onChange={this.changeHandler.bind(this,"remarks")}

 margin="normal"   id="remarks" aria-describedby="emailHelp" placeholder=""/>
      </div>      
                   
                 


                         {/* <Button  style={{marginTop:50}} onClick={this.onsubmittravel} color="success">Sumbit</Button>
                      */}
                                             </div>                  
                    
             
            }
            footer={<Button onClick={this.onsubmittravel} color="primary">Submit</Button>}
          />
        </ItemGrid>
        {/* <ItemGrid xs={12} sm={12} md={4}>
          <ProfileCard
            avatar={avatar}
            subtitle="CEO / CO-FOUNDER"
            title="Alec Thompson"
            description="Don't be scared of the truth because we need to restart the human foundation in truth And I love you like Kanye loves Kanye I love Rick Owens’ bed design but the back is..."
            footer={
              <Button color="primary" round>
                Follow
              </Button>
            }
          />
        </ItemGrid> */}
      </Grid>
    </div>
  );
  }
}
export default TravelRequest;
