import React from "react";
import { Grid,Card,CardContent, InputLabel,SnackbarContent,TextField,FormControlLabel,Checkbox,Dialog,DialogTitle,DialogContent,DialogActions,Slide} from "material-ui";
import classNames from "classnames";
import {
  ProfileCard,
  RegularCard,
  Button,
  CustomInput,
  ItemGrid
} from "components";

function Transition(props) {
    return <Slide direction="down" {...props} />;
  }
class Wallet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          
          form:{
          
          
          },
          usd:'',
          myr:'',
          eur:'',
          aed:'',
          sar:'',
          inr:''
           
      };
    
      }
    
  componentDidMount(){
    
    this.alldatasneeded()
    
  }  

  alldatasneeded(){


    fetch('http://localhost:3022/select/all_cash',
    {
      method:'GET',
      credentials : 'omit',
      headers: {
      
       'Accept': 'application/json',
       'Content-Type': 'application/json',
  
   },
    
    })
    .then(result=>result.json())
    .then((findres)=>{
      console.log(findres)
      
      this.setState({
 
          usd:findres.usd,
          myr:findres.myr,
          eur:findres.eur,
          aed:findres.aed,
          sar:findres.sar,
          inr:findres.inr,
      
     })

  

 
    })
    .catch((error) => {
      
      console.error(error);
 
   }); 
 
 
 


  }
 
  
   





   changeHandler (field, e) {
    
    let form = this.state.form;
    form[field] = e.target.value;
    this.setState(form);
   

    
    
   }
   handleClose(modal) {

    var x = [];
    x[modal] = false;
    this.setState(x);

  }

handleClickOpen(modal,message) {

    console.log("message"+message)
    var x = [];
    x[modal] = true;
    this.setState(x);
    this.setState({
      alertmessage:message
    });

  }
  
  render() {
    const { classes } = this.props;
    return (
      <div >

            <Dialog
                
                fullWidth
              
                open={this.state.classicModal}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => this.handleClose("classicModal")}
                aria-labelledby="classic-modal-slide-title"
                aria-describedby="classic-modal-slide-description"

                >

                <DialogTitle
                id="classic-modal-slide-title"
                disableTypography
            
                >

                    
                <h4 >Info </h4>
                </DialogTitle>

                <DialogContent
                    
                id="classic-modal-slide-description"
             

                >
                <p> {this.state.alertmessage}</p>

 </DialogContent>
         <DialogActions >
           {/* <Button color="transparent" simple>
             Nice Button
           </Button> */}
           <Button
             onClick={() => this.handleClose("classicModal")}
             color="transparent"
             simple
           >
             Close
           </Button>
         </DialogActions>
       </Dialog>

 
       

       

<RegularCard
            cardTitle="Balance Available"
            cardSubtitle="Cash Available on wallet"
            content={

              <div>

              <div STYLE="margin-top:30px" class="form-row">
  
  <div class="form-group col-md-2">

      <div align="center" >
      <h3> {this.state.usd}<br /> USD</h3>
      </div>
  
  </div>
  <div class="form-group col-md-2">
  <div align="center" >
     <h3>{this.state.myr} <br /> MYR  </h3>
      </div>
  </div>
  <div class="form-group col-md-2">
  <div align="center" >
  <h3> {this.state.eur} <br />  EUR</h3>
      </div>
  </div>
  <div class="form-group col-md-2">
  <div align="center" >
  <h3> {this.state.aed} <br /> AED </h3>
      </div>
  </div>
  <div class="form-group col-md-2">
  <div align="center" >
  <h3> {this.state.sar} <br />  SAR </h3>
      </div>
  </div>
  <div class="form-group col-md-2">
  <div align="center" >
  <h3> {this.state.inr} <br /> INR  </h3>
      </div>
  </div>
  </div>
                </div>
            }

            />
     
     
         
      
       
 

      </div>
    );
  }
}

export default Wallet;
