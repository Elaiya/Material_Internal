import React from "react";
import { Grid,Tooltip,IconButton, InputLabel,SnackbarContent,TextField,FormControlLabel,Checkbox,Dialog,DialogTitle,DialogContent,DialogActions,Slide} from "material-ui";
import classNames from "classnames";
import {
  ProfileCard,
  RegularCard,
  Button,
  CustomInput,
  ItemGrid,

} from "components";
import AddCircle from '@material-ui/icons/AddCircle';


function Transition(props) {
    return <Slide direction="down" {...props} />;
  }

var arrayofrows=[]  
class ClaimSheet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          
          form:{
          
          
          
          },
          alertmessage:"",
          addarray:[1],
          allclaiamedrequest:[{emp_name:"Select",
          travel_request_id:""}],
          alltravelrequests:[{emp_name:"Select",
          travel_request_id:""}],
          value: 2,
          paid_amount:"",
      };
      
    
      this.alldatasneeded=this.alldatasneeded.bind(this);  

      this.dataupdate=this.dataupdate.bind(this);  

      }
    
  componentDidMount(){
    
   
    this.alldatasneeded()
    
  }  

  handleChange = (event, value) => {
    this.setState({ value });
  };

  changeHandler_Claim(field, e){



    //console.log("array Length"+this.state.addarray.length)
    let form = this.state.form;
    form[field] = e.target.value;
    this.setState({ form });

   




    
    for(var i=0;i<this.state.addarray.length;i++){

      // this.state.form["usdamount"+i]= +this.state.form["currentamount"+i]  +  + this.state.form["currentamount"+i]

        console.log("currency value"  + this.state.MYR)
  

        
      if(document.getElementById("currencytype"+i).value=="USD"){

        this.state.form["usdamount"+i]= +this.state.form["currentamount"+i]
        this.state.form["current_currency_rate"+i]=1
      }else{

        this.state.form["usdamount"+i]=Number(+this.state.form["currentamount"+i]  *  +this.state.form["current_currency_rate"+i]).toFixed(3)
      }




    //   if(document.getElementById("currencytype"+i).value=="MYR"){

    //     this.state.form["usdamount"+i]= Number(+this.state.form["currentamount"+i]  *  +this.state.MYR).toFixed(3)
    
    //   }
  
    //   if(document.getElementById("currencytype"+i).value=="EUR"){

    //     this.state.form["usdamount"+i]= Number(+this.state.form["currentamount"+i]  *  +this.state.EUR).toFixed(3)
    
    //   }
    //   if(document.getElementById("currencytype"+i).value=="AED"){

    //     this.state.form["usdamount"+i]= Number(+this.state.form["currentamount"+i]  *  +this.state.AED).toFixed(3)
    
    //   }
    //   if(document.getElementById("currencytype"+i).value=="SAR"){

    //     this.state.form["usdamount"+i]= Number(+this.state.form["currentamount"+i]  *  +this.state.SAR).toFixed(3)
    
    //   }
    //   if(document.getElementById("currencytype"+i).value=="INR"){

    //     this.state.form["usdamount"+i]= Number(+this.state.form["currentamount"+i]  *  +this.state.INR).toFixed(3)
    
    //   }
    }


    this.setState({ form });


    // var total=0
    // for(var i=0;i<this.state.addarray.length;i++){

    //   total=
   

    // }


  }
  addrow(action,index){

    console.log("index value"+index)
    if(action=="add"){
      arrayofrows=this.state.addarray 
      arrayofrows.push(1)
       this.setState({
  
          addarray:arrayofrows
  
     })
    }else{

      arrayofrows=this.state.addarray 
       
      console.log("array values"+arrayofrows)
      if (index > -1) {
        var newarray=[]
        newarray=arrayofrows.splice(index, 1);
        this.setState({

          addarray:newarray
       
        })
      //  console.log("spliced rows"+arrayofrows)
      }
       

    }
   
}
  alldatasneeded(){


    fetch('http://localhost:3022/select/allclaimed',
    {
      method:'GET',
      credentials : 'omit',
      headers: {
      
       'Accept': 'application/json',
       'Content-Type': 'application/json',
  
   },
    
    })
    .then(result=>result.json())
    .then((findres)=>{
      console.log(findres)
  
      this.setState({
 
        allclaiamedrequest:findres.claimed,
      
     })
       this.dataupdate()
       console.log(("++++++++++"))
 
    })
    .catch((error) => {
      
      console.error(error);
 
   }); 
 
 


  }

  addrow(action,index){

    console.log("index value"+index)
    if(action=="add"){
      arrayofrows=this.state.addarray 
      arrayofrows.push(1)
       this.setState({
  
          addarray:arrayofrows
  
     })
    }else{

      arrayofrows=this.state.addarray 
       
      console.log("array values"+arrayofrows)
      if (index > -1) {
        var newarray=[]
        newarray=arrayofrows.splice(index, 1);
        this.setState({

          addarray:newarray
       
        })
      //  console.log("spliced rows"+arrayofrows)
      }
       

    }
   
}
onselectemp(){
  //currentamount  currencytype  usdamount

  var empid
  var travelrequestid
  for(var i=0;i<this.state.allclaiamedrequest.length;i++){


   var stringofrecevied=this.state.allclaiamedrequest[i].emp_name  +" id:"+this.state.allclaiamedrequest[i].travel_request_id
   if(document.getElementById("claimedrequest").value == stringofrecevied){

       empid=this.state.allclaiamedrequest[i].emp_id
       travelrequestid=this.state.allclaiamedrequest[i].travel_request_id
       // alert(JSON.stringify(this.state.alltravelrequests[i]))

   }
  } 



  var Requestvaluearray=[]

  var cash_type
  var USD=0
  var MYR=0
  var EUR=0
  var AED=0
  var SAR=0
  var INR=0
  for(var i=0;i<this.state.addarray.length;i++){

    var Jsonpush;

    Jsonpush=JSON.stringify({
      amount:this.state.form["currentamount"+i],           
      currencytype:document.getElementById("currencytype" + i).value,
      usedamount:this.state.form["usdamount"+i],
      current_currency_rate:this.state.form["current_currency_rate"+i]
    })
    Requestvaluearray.push(JSON.parse(Jsonpush))
  



      if(document.getElementById("currencytype"+i).value=="USD"){

        USD= this.state.form["currentamount"+i] 
    
      }

      if(document.getElementById("currencytype"+i).value=="MYR"){

        MYR= this.state.form["currentamount"+i] 
    
      }
  
      if(document.getElementById("currencytype"+i).value=="EUR"){

        EUR= this.state.form["currentamount"+i]     
      }
      if(document.getElementById("currencytype"+i).value=="AED"){

        AED= this.state.form["currentamount"+i]     
      }
      if(document.getElementById("currencytype"+i).value=="SAR"){

        SAR= this.state.form["currentamount"+i]     
      }
      if(document.getElementById("currencytype"+i).value=="INR"){

        INR= this.state.form["currentamount"+i]     
      }


  }
  
  console.log("USD------->>>"+USD)
  console.log("MYR------->>>"+MYR)
  console.log("EUR------->>>"+EUR)
  console.log("AED------->>>"+AED)
  console.log("SAR------->>>"+SAR)
  console.log("INR------->>>"+INR)

  var Claim_sheet=JSON.stringify({
    travelrequestid:travelrequestid,
    empid:empid,
    data:Requestvaluearray,
    createdby:this.props.username,
    createddate:new Date().toISOString().slice(0, 10),
    updatedby:this.props.username,
    updateddate:new Date().toISOString().slice(0, 10),

   })

   var  cash_type
   if(document.getElementById("type_settlement").value=="Claim"){

     cash_type="claim"
     USD=-Math.abs(USD)
     MYR=-Math.abs(MYR)
     EUR=-Math.abs(EUR)
     AED=-Math.abs(AED)
     SAR=-Math.abs(SAR)
     INR=-Math.abs(INR)


  }else{

    cash_type="balance"
  }

   var Claim_sheet_Cash_Details=JSON.stringify({
    travelrequestid:travelrequestid,
    type_cash:cash_type,
    usd:USD,
    myr:MYR,
    eur:EUR,
    aed:AED,
    sar:SAR,
    inr:INR, 
    flag:"claimed",
    createdby:this.props.username,
    createddate:new Date().toISOString().slice(0, 10),
    updatedby:this.props.username,
    updateddate:new Date().toISOString().slice(0, 10),

   })

   fetch('http://localhost:3022/insert/cashupdates',
   {
     method:'POST',
     credentials : 'omit',
     headers: {
     
      'Accept': 'application/json',
      'Content-Type': 'application/json',
 
  },
   body:Claim_sheet_Cash_Details
   })
   .then(result=>result.json())
   .then((findres)=>{
     console.log(findres)
     console.log(("++++++++++"))
 
     if(findres.info=='insert failure'){
 
       this.handleClickOpen("classicModal","Error Uploaded data")
 
     }else{
 
       this.handleClickOpen("classicModal","Successfully Saved")
       this.alldatasneeded()
 
 
       // let form=this.state.form
       // this.state.form.amount=""
       // this.state.form.currencytype=""
       // this.state.form.usedamount=""
      
       for(var i=0;i<this.state.addarray.length;i++){
 
       this.state.form["currentamount"+i]=''   
       this.state.form["currencytype"+i]="USD"       
       this.state.form["usdamount"+i]=''
 
       }
      let form=this.state.form 
      this.setState({form}) 
      this.setState({addarray:[1]})
       
     }
 
   })
   .catch((error) => {
     this.handleClickOpen("classicModal",error)
    console.error(error);
  });

//alert("Request Json"+ Claim_sheet)

//   fetch('http://localhost:3022/insert/travel/claimsheet',
//   {
//     method:'POST',
//     credentials : 'omit',
//     headers: {
    
//      'Accept': 'application/json',
//      'Content-Type': 'application/json',

//  },
//   body:Claim_sheet
//   })
//   .then(result=>result.json())
//   .then((findres)=>{
//     console.log(findres)
//     console.log(("++++++++++"))

//     if(findres.info=='insert failure'){

//       this.handleClickOpen("classicModal","Error Uploaded data")

//     }else{

//       this.handleClickOpen("classicModal","Successfully Saved")
//       this.alldatasneeded()


//       // let form=this.state.form
//       // this.state.form.amount=""
//       // this.state.form.currencytype=""
//       // this.state.form.usedamount=""
     
//       for(var i=0;i<this.state.addarray.length;i++){

//       this.state.form["currentamount"+i]=''   
//       this.state.form["currencytype"+i]="USD"       
//       this.state.form["usdamount"+i]=''

//       }
//      let form=this.state.form 
//      this.setState({form}) 
//      this.setState({addarray:[1]})
      
//     }

//   })
//   .catch((error) => {
//     this.handleClickOpen("classicModal",error)
//    console.error(error);
//  });
  // console.log("Length of array" +Requestvaluearray.length)
  
      


}

  
  dataupdate(){

    var empid
    var travelrequestid

    for(var i=0;i<this.state.allclaiamedrequest.length;i++){


      var stringofrecevied=this.state.allclaiamedrequest[i].emp_name  +" id:"+this.state.allclaiamedrequest[i].travel_request_id
      if(document.getElementById("claimedrequest").value == stringofrecevied){
   
          empid=this.state.allclaiamedrequest[i].emp_id
          travelrequestid=this.state.allclaiamedrequest[i].travel_request_id
          console.log(JSON.stringify(this.state.allclaiamedrequest[i]))
          this.setState({
          
            paid_amount:this.state.allclaiamedrequest[i].vendor_actual_amount_paid  +" "+   this.state.allclaiamedrequest[i].vendor_currency 

          }) 
          
      }
     } 

    
  }



  changeHandler (field, e) {
    
    let form = this.state.form;
    form[field] = e.target.value;
    this.setState({ form });
    this.dataupdate()



   }
   handleClose(modal) {

    var x = [];
    x[modal] = false;
    this.setState(x);

  }

handleClickOpen(modal,message) {

    console.log("message"+message)
    var x = [];
    x[modal] = true;
    this.setState(x);
    this.setState({
      alertmessage:message
    });

  }
  
  render() {
    const { classes } = this.props;
    return (
      <div >

            <Dialog
                
                fullWidth
              
                open={this.state.classicModal}
                TransitionComponent={Transition}
                keepMounted
                onClose={() => this.handleClose("classicModal")}
                aria-labelledby="classic-modal-slide-title"
                aria-describedby="classic-modal-slide-description"

                >

                <DialogTitle
                id="classic-modal-slide-title"
                disableTypography
            
                >

                    
                <h4 >Info </h4>
                </DialogTitle>

                <DialogContent
                    
                id="classic-modal-slide-description"
             

                >
                <p> {this.state.alertmessage}</p>

 </DialogContent>
         <DialogActions >
           {/* <Button color="transparent" simple>
             Nice Button
           </Button> */}
           <Button
             onClick={() => this.handleClose("classicModal")}
             color="transparent"
             simple
           >
             Close
           </Button>
         </DialogActions>
       </Dialog>

  <Grid conainer>
        <ItemGrid xs={12} sm={12} md={16}>
      
      

          <RegularCard
               cardTitle="Amount Paid"
               cardSubtitle={this.state.paid_amount}
            // cardTitle="Balance: 150 USD"
            content={
<div>
<div>
  

   <div class="form-row">
    <div class="form-group col-md-4">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Travel Request</label>
    <select 
       value={this.state.form.claimedrequest}
       onChange={this.changeHandler.bind(this,"claimedrequest")}
       
      class="form-control" id="claimedrequest" aria-describedby="emailHelp"  >
   
   {this.state.allclaiamedrequest.map((row,index ) => {
                                           
                                           return(
                                         
                                             <option>{this.state.allclaiamedrequest[index].emp_name + " id:"+this.state.allclaiamedrequest[index].travel_request_id}</option>
                                             
                                         
                                           ) })}
     
                   
   
      </select>
    </div>
    <div STYLE="margin-left:50px;" class="form-group col-md-3">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Type</label>
    <select 
       value={this.state.form.type_settlement}
       onChange={this.changeHandler.bind(this,"type_settlement")}
       
      class="form-control" id="type_settlement" aria-describedby="emailHelp"  >

      <option>Claim</option>                   
      <option>Excess</option> 
      </select>
    </div>
  </div>
  
{/* <div class="form-group col-md-4">
    <label for="exampleInputEmail1"  STYLE=" font-weight: bold;color:black;">Claimed Request</label>
    <select 
       value={this.state.form.claimedrequest}
       onChange={this.changeHandler.bind(this,"claimedrequest")}
       
      class="form-control" id="claimedrequest" aria-describedby="emailHelp"  >
   
   {this.state.allclaiamedrequest.map((row,index ) => {
                                           
                                           return(
                                         
                                             <option>{this.state.allclaiamedrequest[index].emp_name + " id:"+this.state.allclaiamedrequest[index].travel_request_id}</option>
                                             
                                         
                                           ) })}
     
                   
   
      </select>
      </div> */}

      
{/*                           
                                     <TextField
                                     label="Select Travel Request" 
                                     id="claimedrequest"
                                     select
                                  
                                     
                                     SelectProps={{
                                       native: true,
                                      
                                     }}
                                   
                                     margin="normal"
                                     value={this.state.form.claimedrequest}
                                     onChange={this.changeHandler.bind(this,"claimedrequest")}
                                 >
                     
                               {this.state.allclaiamedrequest.map((row,index ) => {
                                           
                                           return(
                                         
                                             <option>{this.state.allclaiamedrequest[index].emp_name + " id:"+this.state.allclaiamedrequest[index].travel_request_id}</option>
                                             
                                         
                                           ) })}
                                 
                                   </TextField>
                                 
                                   */}
                                   <table style={{marginTop:20}} class="table table-striped">
                                 
                                 <tr>
                                   <th>Amount</th>
                                   <th>Current Rate</th>
                                   <th>Currency Type</th>
                                   <th>USD amount</th>
                                   <th><center>Action</center></th>
                                   </tr>
           
           
                                  {this.state.addarray.map((row,index ) => {
                                     
           
                                     console.log('row'+row)
           
                                     console.log('Index of the value'+index)
                                     return(
                                      
                                       <tr>
           
                                       <td><input type="number"  class="form-control" id="usr" value={this.state.form["currentamount"+index]} onChange={this.changeHandler_Claim.bind(this,"currentamount"+index)} id={"currentamount"+index}/></td>
                                       <td><input type="number"  class="form-control" id="usr" value={this.state.form["current_currency_rate"+index]} onChange={this.changeHandler_Claim.bind(this,"current_currency_rate"+index)} id={"current_currency_rate"+index}/></td>
                               
                                       <td>  <select class="form-control" value={this.state.form["currencytype"+index]} onChange={this.changeHandler_Claim.bind(this,"currencytype"+index)} id={"currencytype"+index}>
               <option>USD</option>
               <option>MYR</option>
               <option>EUR</option>
               <option>AED</option>
               <option>SAR</option>
               <option>INR</option>
             </select></td>
                                   <td><input disabled type="number"  class="form-control" id="usr" value={this.state.form["usdamount"+index]} onChange={this.changeHandler_Claim.bind(this,"usdamount"+index)} id={"usdamount"+index}/></td>
                                
                                       
                                   {/* <td><button type="button" onClick={this.addrow.bind(this,"delete")} class="btn btn-danger">X</button></td>
                            */}
                                    <td><center>
                                    <Tooltip id="tooltip-icon" title="Add">
                   <IconButton 
                    onClick={this.addrow.bind(this,"add",index)}
                    aria-label="Add">
                     <AddCircle />
                   </IconButton>
                 </Tooltip>
                 
                  {/* <Tooltip id="tooltip-icon" title="Delete">
                   <IconButton
                    onClick={this.addrow.bind(this,"delete",index)}
                   aria-label="Delete">
                     <DeleteIcon />
                   </IconButton>
                 </Tooltip> */}
                 </center>
                 </td>
                                       </tr>
           
                  ) })}
           
                    {/* <tr>
           
            
                     <td></td>
                     <td >Total</td>
                     <td>{this.state.form.claimtotal}</td>
                     <td></td>
                   </tr>             */}
                                </table>
           
           
                                 <Button  style={{marginTop:50}} onClick={this.onselectemp.bind(this)}  color="secondary">Sumbit</Button>
                            
           
           
           
           
                            
                                 </div>

</div>

            }
            />
  </ItemGrid >
  </Grid >

      </div>
    );
  }
}

export default ClaimSheet;
